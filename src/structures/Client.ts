import { AkairoClient, CommandHandler, InhibitorHandler, ListenerHandler } from 'discord-akairo';
import { Logger } from './index';
import { join } from 'path';
const { DISCORD_TOKEN } = process.env;


export class Client extends AkairoClient {
	public logger: Logger;
	public commandHandler: CommandHandler;
	public inhibitorHandler: InhibitorHandler;
	public listenerHandler: ListenerHandler;
	constructor(...options: ConstructorParameters<typeof AkairoClient>) {
		super(
            options[0],
            Object.assign({}, options[1], {
                shards: 'auto',
                messageCacheMaxSize: 10,
                messageCacheLifetime: 10000,
                messageSweepInterval: 30000,
                messageEditHistoryMaxSize: 3,
            })
        );
		this.logger = new Logger();
	}

	async start(): Promise<void> {
		this.commandHandler = new CommandHandler(this, {
			directory: join(__dirname, '..', 'commands'),
			prefix: '//'
		});
		this.listenerHandler = new ListenerHandler(this, {
			directory: join(__dirname, '..', 'listeners')
		});
		this.listenerHandler.setEmitters({
			commandHandler: this.commandHandler,
			listenerHandler: this.listenerHandler
		});
		this.commandHandler
			.useListenerHandler(this.listenerHandler)
			.loadAll();
		this.listenerHandler.loadAll();

		await super.login(DISCORD_TOKEN);
        
		const owner = (await super.fetchApplication()).owner!.id;
        this.ownerID = this.commandHandler.ignoreCooldown = owner;
	}
}