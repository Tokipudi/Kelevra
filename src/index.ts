import { config } from 'dotenv';
config();
import { Client } from './structures/Client';
const client = new Client();
client.start();