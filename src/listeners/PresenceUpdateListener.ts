import { Listener } from '@structures';
import { Presence } from 'discord.js';
import { PresenceUtils } from '@utils/PresenceUtils';
import { Guild } from 'discord.js';
import { Activity } from 'discord.js';
import { GuildMember } from 'discord.js';
import { CategoryChannel } from 'discord.js';
import { Channel } from 'discord.js';
import { GuildChannel } from 'discord.js';
import { TextChannel } from 'discord.js';

export default class PresenceUpdateListener extends Listener {
	constructor() {
		super('presenceUpdate', {
			emitter: 'client',
			event: 'presenceUpdate'
		});
	}

	async exec(oldPresence: Presence, newPresence: Presence) {
		let member = newPresence.member;
		let guild = newPresence.guild;

		for (const activity of newPresence.activities) {
			if (activity.type !== 'PLAYING' || !activity.name || !PresenceUtils.hasRichPresence(activity)) continue;

			this.createRole(guild, activity, member);
			this.createChannel(guild, activity);
		}
	}

	async createRole(guild: Guild, activity: Activity, member: GuildMember) {
		// TODO Find out why the fetch is needed to reload the cache
		let roles = await guild.roles.fetch();
		let role = roles.cache.find(r => r.name === activity.name);

		if (role == null) {
			role = await guild.roles.create({
				data: {
					name: activity.name,
					color: 'GREY'
				},
				reason: `Role ${activity.name} created when ${member.user.tag} played it`
			});
		}

		if (!member.roles.cache.has(role.id)) {
			member.roles.add(role)
				.finally(() => console.log(`Role ${role.name} successfully added to user ${member.user.tag}`))
				.catch(console.error);
		}
	}

	async createChannel(guild: Guild, activity: Activity) {
		let channelName = this.formatTextChannelName(activity.name);
		let roleChannel = guild.channels.cache.find(channel => channel.name === channelName);

		if (roleChannel == null) {
			let gameCategoryChannel: CategoryChannel = guild.channels.cache.find(channel => channel.name === 'Games' && channel.type === 'category') as CategoryChannel;

			if (gameCategoryChannel == null) {
				console.log('The category channel "Games" does not exist.');
			} else {
				let newChannel: TextChannel = await guild.channels.create(channelName, { parent: gameCategoryChannel.id })
					.finally(() => console.log(`Channel ${activity.name} successfully created!`))
					.catch(console.error) as TextChannel;
			}
		}
	}

	formatTextChannelName(string: string) {
		return string.toLowerCase().replace(/[^a-z0-9-]/g, ' ').trim().replace(/\s/g, '-').replace(/(-)\1+/g, '$1');
	}
}