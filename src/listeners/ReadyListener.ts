import { Listener } from "@structures";

export default class ReadyListener extends Listener {
    constructor() {
        super('ready', {
            emitter: 'client',
            event: 'ready'
        });
    }

    async exec() {
        this.client.logger.info(`[READY] Kelevra up and ready to go!`);
    }
}