import { Activity } from 'discord.js';

export class PresenceUtils {
	
	static hasRichPresence(activity: Activity) {
		if (activity && activity.assets && activity.assets != null) {
			return true;
		}
		return false;
	}
}